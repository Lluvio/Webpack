export default function(router){

  // set the default router-view
  router.redirect({
    '/postlist': '/postlist/test',
  })
}
